<?php

namespace App\Presenters;

use App\Model\Facade\SchoolRegisterFacade;
use Nette\Application\Responses\JsonResponse;

class ApiPresenter extends \App\Presenters\BasePresenter
{

        /**
         * @var SchoolRegisterFacade $schoolRegister
         * @inject
         */
        public $schoolRegister;
        
        public function renderArticles()
        {
                $array = $this->schoolRegister->GetArticlesArray();
                $this->sendResponse(new JsonResponse($array));
        }

        /**
         * @param int $id
         */
        public function renderArticle($id)
        {
                $article = $this->schoolRegister->GetArticle($id)->toArray();
                $this->sendResponse(new JsonResponse($article));
        }
         
        /**
         * @param int $id
         */
        public function renderArticlesByAuthor($id)
        {
                $array = $this->schoolRegister->GetArticlesArray($id);
                $this->sendResponse(new JsonResponse($array));
        }

}
