<?php

namespace App\Presenters;

use Nette;
use App\Model\Facade\SchoolRegisterFacade;

class HomepagePresenter extends Nette\Application\UI\Presenter
{

        /**
         * @var SchoolRegisterFacade $schoolRegister
         * @inject
         */
        public $schoolRegister;

        public function renderDefault()
        {
                $article = $this->schoolRegister->GetArticle(1);
                
                $author = $article->author->name . ' ' . $article->author->surname;
                $title = $article->name;
                
                $this->template->articleAuthorUpper = $this->schoolRegister->GetArticleTitleAuthor($author, $title, "uppercase");
                $this->template->articleAuthorLower = $this->schoolRegister->GetArticleTitleAuthor($author, $title, "lowercase");
                $this->template->authorArticleUpper = $this->schoolRegister->GetArticleAuthorTitle($author, $title, "uppercase");
                $this->template->authorArticleLower = $this->schoolRegister->GetArticleAuthorTitle($author, $title, "lowercase");
                
                $this->template->students = $this->schoolRegister->GetStudents();
                $this->template->employees = $this->schoolRegister->GetEmployees();
                $this->template->articles = $this->schoolRegister->GetArticles();
        }

        /**
         * @param int $employeesId
         */
        public function handleCalculateNewPay($employeesId)
        {
                $this->schoolRegister->ConversionPay($employeesId);
                if ($this->isAjax())
                {
                        $this->redrawControl("employeers");
                }
                else
                {
                        $this->redirect("this");
                }
        }

}
