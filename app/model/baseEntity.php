<?php

namespace App\Model\Entity;

use Nette;

/**
 * Represents repository for database table
 */
abstract class BaseEntity extends Nette\Object {

    /** @var Nette\Database\Connection */
    protected $db;

    /**
     * @param  \Nette\Database\Context
     * @throws \NetteAddons\InvalidStateException
     */
    public function __construct(Nette\Database\Context $db) {
        if (!isset($this->tableName)) {
            $class = get_called_class();
            throw new \NetteAddons\InvalidStateException("Property \$tableName must be defined in $class.");
        }

        $this->db = $db;
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    protected function getTable() {
        return $this->db->table($this->tableName);
    }

    /**
     * @return \Nette\Database\Table\Selection
     */
    protected function findAll() {
        return $this->getTable();
    }
    
    /**
     * @param  array
     * @return \Nette\Database\Table\Selection
     */
    protected function findFirst() {
        return $this->findAll()->limit(1)->order('id ASC')->fetch();
    }
    
    /**
     * @param  array
     * @return \Nette\Database\Table\Selection
     */
    protected function findLast() {
        return $this->findAll()->limit(1)->order('id DESC')->fetch();
    }
    
    /**
     * @param  array
     * @return \Nette\Database\Table\Selection
     */
    protected function findBy(array $by) {
        return $this->getTable()->where($by);
    }

    /**
     * @param  array
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    protected function findOneBy(array $by) {
        return $this->findBy($by)->limit(1)->fetch();
    }
     /**
     * @param  array
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    protected function findLastBy(array $by) {
        return $this->findBy($by)->limit(1)->order('id DESC')->fetch();
    }
    
    /**
     * @param  int
     * @return \Nette\Database\Table\ActiveRow|FALSE
     */
    protected function find($id) {
        return $this->getTable()->wherePrimary($id)->fetch();
    }

    /**
     * Insert row in database or update existing one.
     *
     * @param  array
     * @return \Nette\Database\Table\ActiveRow automatically found based on first "column => value" pair in $values
     */
    protected function createOrUpdate(array $values) {
        $pairs = array();
        foreach ($values as $key => $value) {
            $pairs[] = "`$key` = ?"; // warning: SQL injection possible if $values infected!
        }

        $pairs = implode(', ', $pairs);
        $values = array_values($values);

        $this->db->queryArgs(
                'INSERT INTO `' . $this->tableName . '` SET ' . $pairs .
                ' ON DUPLICATE KEY UPDATE ' . $pairs, array_merge($values, $values)
        );

        return $this->findOneBy(func_get_arg(0));
    }

}