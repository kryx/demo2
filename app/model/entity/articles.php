<?php

namespace App\Model\Entity;

/**
 * @property-read int $id
 * @property int $author_id
 * @property text $text
 */
class Articles extends BaseEntity
{

        /** @var string */
        protected $tableName = 'articles';

        /**
         * @param int $articleId
         */
        public function GetArticle($articleId)
        {
                return $this->find($articleId);
        }
        
        public function GetAllArticles()
        {
                return $this->findAll();
        }

        /**
         * @param int $authorId
         */
        public function GetArticlesByAuthor($authorId)
        {
                return $this->findBy(["author_id" => $authorId]);
        }

        /**
         * @param int $authorId
         */
        public function CountArticlesByAuthor($authorId)
        {
                return $this->GetArticlesByAuthor($authorId)->count("*");
        }

}
