<?php

namespace App\Model\Entity;

/**
 * @property-read int $id
 * @property string $name
 * @property string $surname
 */
class Students extends BaseEntity
{

        /** @var string */
        protected $tableName = 'students';

        /**
         * @param int $studentId
         */
        public function GetStudent($studentId)
        {
                return $this->find($studentId);
        }

        public function GetAllStudents()
        {
                return $this->findAll();
        }

}
