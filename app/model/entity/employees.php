<?php

namespace App\Model\Entity;

/**
 * @property-read int $id
 * @property string $name
 * @property string $surname
 * @property string $job
 * @property int $pay
 */
class Employees extends BaseEntity
{

        /** @var string */
        protected $tableName = 'employees';

        /**
         * @param int $employeeId
         */
        public function GetEmployee($employeeId)
        {
                return $this->find($employeeId);
        }

        public function GetAllEmployees()
        {
                return $this->findAll();
        }

        /**
         * @param string $job
         */
        public function GetEmployeesByJob($job)
        {
                return $this->findBy(["job" => $job]);
        }

        /**
         * @param array $values
         */
        public function EditEmployee(array $values)
        {
                return $this->createOrUpdate($values);
        }

}
