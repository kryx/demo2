<?php

namespace App\Model;

class ConversionPay
{

        private $implementation = NULL;

        /**
         * @param string $job
         */
        public function __construct($job)
        {
                switch ($job) {
                        case "Ředitel":
                                $this->implementation = new StrategyReditel();
                                break;
                        case "Děkan":
                                $this->implementation = new StrategyDekan();
                                break;
                        case "Doktorand":
                                $this->implementation = new StrategyDoktorand();
                                break;
                        case "LVLStandaNechutny":
                                $this->implementation = new StrategyLVLStandaNechutny();
                                break;
                        default:
                                throw new InvalidArgumentException('Bad argument');
                }
        }

        /**
         * @param int $pay
         */
        public function CalculateNewPay($pay)
        {
                return $this->implementation->CalculatePay($pay);
        }

}

interface IConversionPay
{

        /** @return ConversionPay */
        function create($job);
}

interface PayInterface
{
        /**
         * @param int $pay
         */
        public function CalculatePay($pay);
}

/**
 * Czech class name only for better understanding
 */
class StrategyReditel implements PayInterface
{

        /**
         * @param int $pay
         */
        public function CalculatePay($pay)
        {
                /**
                 * any algorithm for calculating a new income
                 */
                $pay += rand(5000, 10000);
                return $pay;
        }

}

/**
 * Czech class name only for better understanding
 */
class StrategyDekan implements PayInterface
{

        /**
         * @param int $pay
         */
        public function CalculatePay($pay)
        {
                /**
                 * any algorithm for calculating a new income
                 */
                $pay += rand(2000, 5000);
                return $pay;
        }

}

/**
 * Czech class name only for better understanding
 */
class StrategyDoktorand implements PayInterface
{

        /**
         * @param int $pay
         */
        public function CalculatePay($pay)
        {
                /**
                 * any algorithm for calculating a new income
                 */
                $pay += rand(1000, 2000);
                return $pay;
        }

}

class StrategyLVLStandaNechutny implements PayInterface
{

        /**
         * @param int $pay
         */
        public function CalculatePay($pay)
        {
                /**
                 * He has a gun !!!! And i want to survive.. So this is the best way!
                 */
                $pay += 1000000;
                return $pay;
        }

}
