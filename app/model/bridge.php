<?php

namespace App\Model;

abstract class ArticleShow
{

        private $author;
        private $article;
        private $implementation;

        /**
         * @param string $author  author name
         * @param string $article article name
         * @param string $case    letter case
         */
        public function __construct($author, $article, $case)
        {
                $this->author = $author;
                $this->article = $article;

                switch (strtolower($case)) {
                        case "uppercase":
                                $this->implementation = new ArticleUppercase();
                                break;
                        case "lowercase":
                                $this->implementation = new ArticleLowercase();
                                break;
                        default:
                                $this->implementation = new ArticleStandard();
                                break;
                }
        }

        protected function showAuthor()
        {
                return $this->implementation->showAuthor($this->author);
        }

        protected function showArticle()
        {
                return $this->implementation->showArticle($this->article);
        }

}

class AuthorArticle extends ArticleShow
{

        public function showAuthorArticle()
        {
                return "Autor \"" . $this->showAuthor() . "\" napsal článek \"" . $this->showArticle() . "\"";
        }

}

interface IBridgeAuthorArticle
{

        /** @return AuthorArticle */
        function create($author, $article, $case);
}

class ArticleAuthor extends ArticleShow
{

        public function showArticleAuthor()
        {
                return "Článek \"" . $this->showArticle() . "\" byl napsán autorem \"" . $this->showAuthor() . "\"";
        }

}

interface IBridgeArticleAuthor
{

        /** @return ArticleAuthor */
        function create($author, $article, $case);
}

abstract class ArticleImplementation
{

        /**
         * @param string $author 
         */
        abstract function showAuthor($author);

        /**
         * @param string $article 
         */
        abstract function showArticle($article);
}

class ArticleStandard extends ArticleImplementation
{

        /**
         * @param string $author 
         */
        public function showAuthor($author)
        {
                return $author;
        }

        /**
         * @param string $article
         */
        public function showArticle($article)
        {
                return $article;
        }

}

class ArticleUppercase extends ArticleImplementation
{

        /**
         * @param string $author 
         */
        public function showAuthor($author)
        {
                return strtoupper($author);
        }

        /**
         * @param string $article 
         */
        public function showArticle($article)
        {
                return strtoupper($article);
        }

}

class ArticleLowercase extends ArticleImplementation
{

        /**
         * @param string $author 
         */
        public function showAuthor($author)
        {
                return strtolower($author);
        }

        /**
         * @param string $article 
         */
        public function showArticle($article)
        {
                return strtolower($article);
        }

}
