<?php

namespace App\Model\Facade;

use App\Model;

class SchoolRegisterFacade
{

        /**
         * @var Model\Entity\Articles 
         */
        private $articles;

        /**
         * @var Model\Entity\Students 
         */
        private $students;

        /**
         * @var Model\Entity\Employees 
         */
        private $employees;

        public function __construct(Model\Entity\Articles $articles, Model\Entity\Employees $employees, Model\Entity\Students $students)
        {
                $this->articles = $articles;
                $this->students = $students;
                $this->employees = $employees;
        }

        /**
         * @param int $articleId
         */
        public function GetArticle($articleId)
        {
                return $this->articles->GetArticle($articleId);
        }

        /**
         * @param string $author  author name
         * @param string $title   article title
         * @param string $case    letter case
         */
        public function GetArticleAuthorTitle($author, $title, $case)
        {
                $article = new Model\AuthorArticle($author, $title, $case);

                return $article->showAuthorArticle();
        }

        /**
         * @param string $author  author name
         * @param string $title   article title
         * @param string $case    letter case
         */
        public function GetArticleTitleAuthor($author, $title, $case)
        {
                $article = new Model\ArticleAuthor($author, $title, $case);

                return $article->showArticleAuthor();
        }

        /**
         * @param int $authorId
         */
        public function GetArticles($authorId = null)
        {
                if ($authorId != null)
                {
                        $articles = $this->articles->GetArticlesByAuthor($authorId);
                }
                else
                {
                        $articles = $this->articles->GetAllArticles();
                }

                return $articles;
        }

        /**
         * @param int $authorId
         */
        public function GetArticlesArray($authorId = null)
        {
                $articles = $this->GetArticles($authorId);

                return $this->ToArray($articles);
        }

        /**
         * @param int $studentId
         */
        public function GetStudent($studentId)
        {
                return $this->students->GetStudent($studentId);
        }

        public function GetStudents()
        {
                return $this->students->GetAllStudents();
        }

        /**
         * @param string $job
         */
        public function GetEmployees($job = null)
        {
                if ($job != null)
                {
                        $employees = $this->employees->GetEmployeesByJob($job);
                }
                else
                {
                        $employees = $this->employees->GetAllEmployees();
                }

                return $employees;
        }

        /**
         * @param int $employeeId
         */
        public function GetEmployee($employeeId)
        {
                return $this->employees->GetEmployee($employeeId);
        }

        /**
         * @param int $employeeId
         */
        public function ConversionPay($employeeId)
        {
                $employee = $this->GetEmployee($employeeId);

                if ($employee == null)
                {
                        return;
                }

                $conversionPay = new Model\ConversionPay($employee->job);
                $newPay = $conversionPay->CalculateNewPay($employee->pay);

                $this->employees->EditEmployee(array(
                        "id" => $employee->id,
                        "pay" => $newPay
                ));
        }

        /**
         * @param object $object
         */
        public function ToArray($object)
        {
                $array = array();

                foreach ($object as $item) {
                        $array[] = $item->toArray();
                }

                return $array;
        }

}
